#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    cam(0)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Widget::update);
    timer->start(40);
}

void Widget::update()
{
    cv::Mat image;
    cam >> image;
    cv::cvtColor(image, image, CV_BGR2RGB);
    QImage qimage(image.data, image.cols, image.rows,
                  image.step, QImage::Format_RGB888);
    ui->label->setPixmap(QPixmap::fromImage(qimage));
}

Widget::~Widget()
{
    delete ui;
}
