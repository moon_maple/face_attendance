#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <opencv2/opencv.hpp>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void update();

private:
    Ui::Widget *ui;
    cv::VideoCapture cam;
    QTimer* timer;
};

#endif // WIDGET_H
